import java.awt.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Implements Ship interface for firing method
 * Defines what the enemy ship is and how it operates
 */
public class Enemy extends GameObject implements Ship, Serializable {

	private static final long serialVersionUID = 6037236323540109415L;

	private transient Image img;
	private int movement = 2;


	public static HashSet<Laser> laserList = new HashSet<Laser>();

    /**
     *Constructs and initialises enemy ship
     * @param imageLocation location for enemy image on local machine
     * @param position positon on canvas of enemy
     * @param size size of enemy ship object
     */
	public Enemy(String imageLocation, Point position, Dimension size) {
		super(imageLocation, position, size);
		GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();

		try {
			img = GameCanvas.getImage(imageLocation, gc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    /**
     *
     * @return returns true if enemies have hit edge of screen and need to change direction
     */
	public boolean switchDirection() {
		if ((getPosition().x + getSize().width) > SpaceInvaders.WIDTH || (getPosition().x <= 0)) {
			SpaceInvaders.setDirection(SpaceInvaders.getDirection() * -1);
			return true;
		}
		return false;
	}


    /**
     *
     * @param x x co-ordinate
     * @param y y co-ordinate
     */
	@Override
	public void move(int x, int y) {
		getPosition().x += (SpaceInvaders.getDirection()) * movement;
		getPosition().y += y;
		getRectangle().setRect(getPosition().x, getPosition().y, getSize().width, getSize().height);
	}

    /**
     * Creates new enemy laser and sets speed
     */
	@Override
	public void fire() {
		Point pos = new Point(getPosition());
		pos.x = pos.x + getSize().width/2;

		Laser l = new Laser(pos, Color.red);
		l.setPosition(new Point(pos.x, pos.y + 40));
		l.setySpeed(-8);//speed of enemy laser

		GameCanvas.addGameObject(l);
		laserList.add(l);
	}

    /**
     * Draws enemy ship at location
     * @param g
     */
	@Override
	public void draw(Graphics g) {
		Point position = getPosition();
		Dimension size = getSize();

		g.drawImage(img, position.x, position.y, size.width, size.height, null);
	}
}