import java.util.ArrayList;

/**
 * Wrapper fitness class for storing the average fitness of each generation's organisms
 * @author Stuart Melrose
 */
public class Fitness {

    public int generation;
    public double fitness;

    /**
     *
     * @param Generation generation number
     * @param Fitness average fitness for generation
     */

    public Fitness(int Generation, Double Fitness){
        this.generation = Generation;
        this.fitness = Fitness;
    }

}
