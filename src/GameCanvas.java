import com.sun.org.apache.xpath.internal.operations.Bool;
import jneat.Neat;
import jneat.Organism;
import jneat.Species;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Point;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Holds majority of functionality for game
 * Creates the game and plays it
 * Defines user actions, Random AI actions, and feeding the Neural Network
 */

public class GameCanvas extends JPanel implements ActionListener{
    private static transient GameCanvas gameCanvas = null;
    private static final int NUMBER_ENEMIES = 28;
    private Player player;
    private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
    private ArrayList<Laser> enemyLaserCleanUpList = new ArrayList<Laser>();
    private ArrayList<Laser> playerLaserCleanUpList = new ArrayList<Laser>();
    private static HashSet<GameObject> gameObjects = new HashSet<GameObject>();
    private int gameCount = 1;
    private static HashMap<String, Image> imageCache = new HashMap<String, Image>();
    private long lastFire;
    private int enemyCount = NUMBER_ENEMIES;
    private Timer leftPressed = new Timer(10, new LeftPressed());
    private Timer rightPressed = new Timer(10, new RightPressed());
    private Timer gameTimer = new Timer(20, this);
    private Timer enemyFireTimer = new Timer(500, new MyFireListener());//rate enemies shoot at
    private Timer respawn = new Timer(100, new redrawPlayer());
    private jNeat neat = new jNeat(this);
    private int orgSize;
    private Vector<Organism> neatOrgs;
    private int curOrg;
    private int generation = 0;
    private double fitness;
    private ArrayList<Fitness> avgFitness = new ArrayList<>();


    private GameCanvas() {
        super();

        Neat.initbase();
        neat.createPop();

        neatOrgs = neat.neatPop.getOrganisms();
        orgSize = neatOrgs.size();
        curOrg = 0;

        setBackground(Color.WHITE);
        setDoubleBuffered(true);
        setFocusable(true);
        initialiseShips();
        gameTimer.start();
        enemyFireTimer.start();
        this.setBackground(Color.BLACK);

        createPlayer();

        //executor service to call laser method every x milliseconds
        ScheduledExecutorService execService = Executors.newScheduledThreadPool(1);
        execService.scheduleAtFixedRate(getLaser, 0, 100, TimeUnit.MILLISECONDS);

    }

    /**
     *
     * @param whichCanvas Determines if there is already an active canvas
     * @return returns a gamecanvas instance
     */
    public static GameCanvas getGameCanvas(boolean whichCanvas) {
        if (gameCanvas == null && whichCanvas == false) {
            gameCanvas = new GameCanvas();
        }
        return gameCanvas;
    }

    private void initialiseShips() {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j<4; j++)
            {
                Enemy e = new Enemy("/Users/stuartmelrose/Desktop/4th year/Honours Project/honours/src/Resources/Enemy.jpg", new Point(100 + i*50, (50)+j*30), new Dimension(50, 30));
                enemies.add(e);
                gameObjects.add(e);
            }
        }
    }

    /**
     *
     * @return returns instance of player object
     */
    public Player getPlayer() {
        return player;
    }

    private void createPlayer() {
        player = new Player("/Users/stuartmelrose/Desktop/4th year/Honours Project/honours/src/Resources/Ship.png", new Point(300, 500), new Dimension(60, 30));
        gameObjects.add(player);
    }

    /**
     * Method that draws all game objects to the JFrame
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.WHITE);
        g.drawString("Fitness:  " + fitness, 20, 30);
        g.drawString("Game no: " + gameCount, 225, 30);
        g.drawString("Organism " + curOrg, 330, 30);
        g.drawString("Generation " + generation, 450,30);

        Iterator<GameObject> itt = gameObjects.iterator();

        while (itt.hasNext()) {
            GameObject current = itt.next();
            if (!current.isActive())
                continue;
            current.draw(g);
        }
    }

    /**
     *
     * @param event when something happens in the game perform actions
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        int moveY = 0;

        for (int i = 0; i < enemies.size(); i++) {
            if (enemies.get(i).switchDirection()) {
                moveY = 0;
                break;
            }
            else
                moveY = 0;
        }

        Iterator<GameObject> itt = gameObjects.iterator();

        while (itt.hasNext()) {
            GameObject current = itt.next();

            if (current instanceof Player)
                continue; // we don't want the player to be automatically updated

            current.move(0, moveY);
        }
        checkLasers();
        collision();
        checkSides();
        cleanUp();
        repaint();

    }

    /**
     *
     * @param obj adds object to gameObjects list
     */
    public static void addGameObject(GameObject obj) {
        gameObjects.add(obj);
    }

    //method that test's for collisions between lasers and ships
    private void collision() {

        boolean restartGame = false;

        // test players for hitting enemy
        for (Enemy e : enemies) {

            Iterator<Laser> laserItt = Player.laserList.iterator();
            while (laserItt.hasNext()) {
                Laser currentLaser = laserItt.next();

                //if laser hits enemy ship kill enemy
                if (currentLaser.getRectangle().intersects(e.getRectangle()) && currentLaser.isActive()) {
                    currentLaser.setActive(false);
                    playerLaserCleanUpList.add(currentLaser);
                    e.setActive(false);
                    enemyCount--;
                }
            }
            if (enemyCount == 0) {
                restartGame = true;
                enemyCount = NUMBER_ENEMIES;
            }
        }

        // test enemy laser hitting player
        Iterator<Laser> laserItt = Enemy.laserList.iterator();
        while (laserItt.hasNext()) {
            Laser currentLaser = laserItt.next();

            //if player is hit
            if (player.isActive()) {
                if (currentLaser.getRectangle().intersects(player.getRectangle()) && currentLaser.isActive()) {
                    currentLaser.setActive(false);
                    enemyLaserCleanUpList.add(currentLaser);
                    enemyFireTimer.stop();
                    gameTimer.stop();
                    respawn.start();
                    player.setActive(false);

                    gameCount++;

                    restartGame = true;
                }

                //if time to move to next organism
                if (gameCount == 4) {

                    Organism curId = ((Organism)neatOrgs.get(curOrg));

                    //if last organism in the population
                    if(curOrg == orgSize - 1){

                        System.out.println("End of Generation " + generation + "\n\n");

                        //set organism fitness
                        if(fitness < 0){
                            fitness = 0;
                            curId.setFitness(fitness);
                        }
                        else{
                            curId.setFitness(fitness);
                        }

                        neat.neatPop.viewtext();

                        //add populations average fitness for this generation to list
                        Double totalFit = 0.0;
                        for(Organism org : neatOrgs){
                            totalFit = totalFit + org.getFitness();
                        }
                        Double avgFit = totalFit/orgSize;
                        Fitness orgFit = new Fitness(generation,avgFit);

                        avgFitness.add(orgFit);

                        neat.neatPop.epoch(generation++);

                        //if last generation of simulation print averages for each generation and quit
                        if(generation == 30){
                            System.out.println("\n\nEnd of Sim\n\n");
                            for (Fitness f : avgFitness){
                                System.out.println("Generation " + f.generation + ", Average Fitness: " + f.fitness);
                            }
                            System.exit(0);
                        }
                        gameCount = 1;
                        neatOrgs = neat.neatPop.getOrganisms();
                        orgSize = neatOrgs.size();
                        curOrg = 0;

                        restartGame = true;

                    }
                    else{
                        //if not last organism in population set fitness and move to next organism
                        if(fitness < 0){
                            fitness = 0;
                            curId.setFitness(fitness);
                        }
                        else{
                            curId.setFitness(fitness);
                        }

                        curOrg++;

                        gameCount = 1;

                        restartGame = true;
                    }
                    fitness = 0;
                }
            }
        }

        if(restartGame == true){
            clear();
            restartGame();
        }
    }

    private void checkSides(){
        //player bounces back from wall if hit
        if(player.getPosition().getX() <5){
            bounce("Left");
        }
        else if(player.getPosition().getX() > 545){
            bounce("");
        }
    }

    //gets random number(distance) to bounce and moves player
    private void bounce(String dir){
        fitness = fitness - 5;
        Random rand = new Random();
        int  n = rand.nextInt(250) + 50;
        if(dir == "Left"){
            player.move(player.getPosition().x+n,0);
        }
        else{
            player.move(player.getPosition().x-n,0);
        }
    }

    //method that removes killed enemies
    private void cleanUp() {
        ArrayList<Enemy> al = new ArrayList<Enemy>();
        for (Enemy e : enemies) {
            if (!e.isActive())
                al.add(e);
        }
        enemies.removeAll(al);
        Player.laserList.removeAll(playerLaserCleanUpList);
        Enemy.laserList.removeAll(enemyLaserCleanUpList);
        playerLaserCleanUpList.clear();
        enemyLaserCleanUpList.clear();
    }

    public static Image getImage(String location, GraphicsConfiguration gc) throws IOException {
        Image img = null;
        if (imageCache.containsKey(location))
            return imageCache.get(location);
        else {
            Image sourceImg;
            sourceImg = ImageIO.read(new File(location));
            img = gc.createCompatibleImage(sourceImg.getWidth(null), sourceImg.getHeight(null), Transparency.BITMASK);
            img.getGraphics().drawImage(sourceImg, 0, 0, null);
            imageCache.put(location, img);
        }
        return img;
    }

    /**
     * Get's an enemy ship and fires from it's position
     */
    public class MyFireListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            if (enemies.size() == 0)
                return;
            else {
                int randomNumber = new Random().nextInt(enemies.size());
                enemies.get(randomNumber).fire();
            }
        }
    }

    //move right
    class RightPressed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (player.isActive() && player.getPosition().getX() < 550)
                player.move(player.getPosition().x+5, 0);
        }

    }

    //move left
    class LeftPressed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (player.isActive() && player.getPosition().getX() > 0)
                player.move(player.getPosition().x-5, 0);
        }
    }

    class redrawPlayer implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0) {
            respawn.stop();

            if (gameCount > 0) {
                createPlayer();
                player.setActive(true);
                enemyFireTimer.start();
                gameTimer.start();
            }
        }
    }

    private void clear(){
        player.setActive(false);
        enemies.clear();
        gameTimer.stop();
        enemyFireTimer.stop();
        gameObjects.clear();
        enemyCount = NUMBER_ENEMIES;
    }

    private void restartGame(){
        initialiseShips();
        gameTimer.start();
        enemyFireTimer.start();
        player.setActive(true);
    }

    //feeds network
    private void feedNetwork(Double[] inputs, Organism org){
        Neat.initbase();
        neat.input(inputs,org);
    }

    //gets the position of the closest laser relative to ship
    private Runnable getLaser = new Runnable(){
        public void run(){

            Point shipPos = player.getPosition();//ships position
            Laser minLaser = null;//initially no closest laser
            Double posFromShip;//distance of laser from ship
            Double[] inputs;

            try{
                if(Enemy.laserList.isEmpty()){
                    return;
                }
                for (Iterator<Laser> laserItt = Enemy.laserList.iterator(); laserItt.hasNext();) {
                    Laser currLaser = laserItt.next();
                    Point laspoint = currLaser.getPosition();

                    //checks if laser is in certain range and if so if it's closest to the ship
                    if(laspoint.getX() > shipPos.getX()-100 && laspoint.getX() < shipPos.getX()+160){
                        if(minLaser == null){
                            minLaser = currLaser;
                        }
                        else if (laspoint.getY() > minLaser.getPosition().getY()){
                            minLaser = currLaser;
                        }
                    }
                }

                if(minLaser == null){
                    return;
                }
                else{
                    //gets position of closest laser and normalises value before feeding the network
                    posFromShip = minLaser.getPosition().getX() - shipPos.getX();
                    inputs = normalise(posFromShip,0.0);
                    //System.out.println("X from ship: " + posFromShip + "\n");
                    feedNetwork(inputs,(Organism) neat.neatPop.organisms.elementAt(curOrg));
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    };

    //method for checking if laser has went out of range, also check if it was close to the player and add fitness if so
    private void checkLasers(){
        Point shipPos = player.getPosition();
        for (Laser currLaser : Enemy.laserList){
            Point lasPoint = currLaser.getPosition();
            if(lasPoint.getY() > 500){
                enemyLaserCleanUpList.add(currLaser);
                if (lasPoint.getX() > shipPos.getX()-150 && lasPoint.getX() < shipPos.getX() + 210){
                    fitness = fitness + 15;
                }
            }
        }
    }

    //normalises values to within range for Neural Network to understand
    private Double[] normalise(Double xPos,Double shipPos){//max x of 600, min of 0
        Double[] inputs = new Double[3];
        inputs[0] = 0.0;
        inputs[1] = 0.0;
        inputs[2] = 0.0;
        //returns single positive number between 0 and 1, input[0] if laser on left and input[1] if on right
        if(xPos < 0){
            inputs[0] = -xPos/600;
        }
        else{
            inputs[1] = xPos/600;
        }

        Boolean enemyAbove = false;
        for(Enemy e : enemies){
            if(e.getPosition().getX() < player.getPosition().getX() + 100 && e.getPosition().getX() > player.getPosition().getX() -10 ){
                enemyAbove = true;
            }
        }

        if(enemyAbove == true){
            //System.out.println("Enemy above");
            inputs[2] = 0.05;
        }

        return inputs;
    }

    /**
     * Random AI method
     */
    private void moveCharacter(){
        int delay = 150;
        Random rand = new Random();
        float moveRandom = round(rand.nextFloat(),1);
        if(moveRandom < 0.5){
            leftPressed.start();
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            leftPressed.stop();
        }else {
            rightPressed.start();
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            rightPressed.stop();
        }
    }

    //round method for random AI float creation
    private static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}