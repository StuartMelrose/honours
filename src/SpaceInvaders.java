//3 inputs 3 outputs,
//inputs - aliens on left, right and lasers
//outputs - move left,right or fire
//fitness function = how many aliens killed

//wrapper class with methods to get amount of aliens killed, when the end of the game is everything we need to train the network

import jneat.Neat;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferStrategy;
import java.io.Serializable;

import javax.swing.*;

/**
 * Main class that creates JFrame and starts game
 */
public class SpaceInvaders extends JFrame implements Serializable {
	private static final long serialVersionUID = 6037236323540109415L;
	public static final int WIDTH = 600;
	public static final int HEIGHT = 600;
	BufferStrategy strategy;
	Container c = getContentPane();
	private static int direction = 1;

	private static final String TITLE = "Space Invaders";

	/**
	 *
	 */
	public SpaceInvaders() {
		super(TITLE);
		final JPanel startPanel = new JPanel();
		startPanel.setBackground(Color.BLACK);
		JButton start = new JButton("Start");
		JButton exit = new JButton("Exit");
		startPanel.add(start);
		startPanel.add(exit);
		this.add(startPanel);

		ImageIcon pic = new ImageIcon("/Users/stuartmelrose/Desktop/4th year/Honours Project/honours/src/Resources/Enemy.jpg");
		Image img = pic.getImage();
		setVisible(true);
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setIgnoreRepaint(true);
		createBufferStrategy(2);

		start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				c.removeAll();
				JFrame frame = SpaceInvaders.this;
				c.add(GameCanvas.getGameCanvas(false));
				GameCanvas.getGameCanvas(false).grabFocus();
				frame.setContentPane(c);
				frame.repaint();
			}
		});

		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		strategy = getBufferStrategy();
		strategy.show();

		setIconImage(img);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new SpaceInvaders();
			}
		});
	}

	public static int getDirection() {
		return direction;
	}

	public static void setDirection(int direction) {
		SpaceInvaders.direction = direction;
	}
}