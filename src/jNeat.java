import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import jneat.*;

import javax.swing.*;

//fitness function where amount of lasers dodged is taken into consideration as well as score, comparison between the two
//get some results
//look into t test statistical significance
//run over set amount of replications each with different parameters and compare differences
//uml class diagram
//user documentation appendix
//java doc for public methods and at the head of each class

/**
 * @author stuartmelrose Class that actually runs the Neural Network
 */

public class jNeat {

    private GameCanvas canvas;
    private Player player;
    private Timer leftPressed = new Timer(10, new LeftPressed());
    private Timer rightPressed = new Timer(10, new RightPressed());

    /**
     * Makes sure correct game canvas is being controlled
     * @param Canvas
     */
    public jNeat(GameCanvas Canvas){
        this.canvas = Canvas;
    }

    Population neatPop;
    int numInputs = 2;

    /**
     * Creates the population for Network
     * Parameters can be altered if need be
     */
    public void createPop(){
        neatPop = new Population(10 , 2, 2, 5, true, 0.5);
    }

    /**
     * Takes input values when called and loads them into the network recieving output
     * Player is controlled using output values
     * @param input normalised input values from GameCanvas class when called
     * @param org Which organism the simulation is on
     */
    public void input(Double [] input, Organism org){
        //System.out.println("Inputs: " + input[0] + ", " + input[1] + ", " + input[2] + "\n");
        Vector<Organism> neatOrgs = neatPop.getOrganisms();
        player = canvas.getPlayer();

        Network brain = ((Organism) neatOrgs.get(org.genome.genome_id)).getNet();

        double inputs[] = new double[numInputs + 1];
        //First input for laser left, second laser right, third for bias
        inputs[numInputs] = -1.0;//bias
        inputs[0] = input[0];
        inputs[1] = input[1];

        brain.load_sensors(inputs);

        int net_depth = brain.max_depth();
        // first activate from sensor to next layer....
        brain.activate();

        // next activate each layer until the last level is reached
        for (int relax = 0; relax <= net_depth; relax++)
        {
            brain.activate();
        }

        // Retrieve outputs from the final layer.
        double output1 = ((NNode) brain.getOutputs().elementAt(0)).getActivation();
        double output2 = ((NNode) brain.getOutputs().elementAt(1)).getActivation();

        int comp = Double.compare(output1,output2);

        int delay = 60;
        if(comp > 0){
            leftPressed.start();
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            leftPressed.stop();
        }else if(comp < 0){
            rightPressed.start();
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            rightPressed.stop();
        }
    }

    //move right
    class RightPressed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (player.isActive() && player.getPosition().getX() < 550)
                player.move(player.getPosition().x+5, 0);
        }

    }

    //move left
    class LeftPressed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (player.isActive() && player.getPosition().getX() > 0)
                player.move(player.getPosition().x-5, 0);
        }
    }
}